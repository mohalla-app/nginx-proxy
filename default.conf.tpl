# nginx config file template
# Will be passed through envsubst to populate the environment variables


server {
	listen ${LISTEN_PORT};

	# We want to serve static files as efficiently as possible, and not via Django
	# So configure nginx to serve static files directly.
	# Note that the Django app should be configured to serve static files from /static
	# If any request comes to /static, serve it directly
	location /static {
		# serve from this location
		# Note: make sure that this volume is actually mounted
		alias /vol/static;
	}

	# Forward any other request to our app
	location / {
		uwsgi_pass                ${APP_HOST}:${APP_PORT};

		# params: https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html#what-is-the-uwsgi-params-file
		# Note: ensure that this file is moved to the correct location
		include                   /etc/nginx/uwsgi_params;

		# Since we will be uploading images, we're keeping the max-size to a reasonable 30Mb.
		# If we wish to serve larger files, we should change this
		# Note: if you're getting the error "entity body size too large", increase the below value
		client_max_body_size      30M;
	}
}
