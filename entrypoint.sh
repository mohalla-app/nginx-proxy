#!/bin/sh

#
# this script will be run automatically inside our docker container
#


# if any part of the script fails, exit immideately and return a failure (non-zero code).
# Helpful for debugging
set -e

# replace the template's placeholders with our environment values
# note: ensure default.conf.tpl is at the appropriate location
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# start the nginx service.
# prevent it from running as a background daemon, because docker apps
# must be run in foreground, so that logs and output are printed to the
# docker output
nginx -g 'daemon off;'
