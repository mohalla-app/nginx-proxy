# ensures that the nginx proxy is run as a non root user for maximum security
# alpine is the most lightweight version of linux for docker containers
# https://hub.docker.com/r/nginxinc/nginx-unprivileged
FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="pragy@mohalla.app"

# copy the files from our repo to the appropriate locations in the container
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

# set the environment variables
ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# drop to the root user to create the directories
USER root
# nginx will serve the static files from /vol/static
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
# allow nginx user running entrypoint.sh to create and modify these files
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
# copy the entrypoint script
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# switch back to nginx user, so that when the build is finished, the last user is nginx
USER nginx

# set container to run the entrypoint script by default
CMD ["/entrypoint.sh"]
